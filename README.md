## GitEdge
#### A Exponent mobile application. 
   
## Getting Started    
#### Install Expo-CLI    
npm install --global expo-cli    
#### clone the project    
git clone https://github.com/maze-runnar/GitEdge.git     
#### install dependencies   
npm install or expo install    
#### start the server   
expo start   

The app is Using github api V3. Currently the app is fetching User persional information as name , profile picture, email, following , followers , repositories and contribution details. The app also have the  ```dark``` and ```light``` mode feature. <br/>
Also I have added tutorial videos, docs, and Books in the app with full fledge git integration.And Also gitlab API integration with App is Work in progress. I am hoping to complete it soon.   
The App is live on expo store and in production mode.
Download APK file from: https://expo.io/artifacts/322016e5-8e55-4a0f-9bab-ab6da50cb832
### Scan to open
Scan this QR code with your expo app.  
<a href="https://imgbb.com/"><img src="https://i.ibb.co/K96s7kc/Awesome-Git.png" alt="Awesome-Git" border="0"></a>

V2:  
<img src="https://i.ibb.co/gyS0SDV/splash.jpg" alt="splash" border="0" width = "200px">
<img src="https://i.ibb.co/cwcMtpw/Screenshot-20200714-074725.png" alt="Screenshot-20200714-074725" border="0" width="200px">
<a href="https://ibb.co/TPZ88Q8"><img src="https://i.ibb.co/5sd88Q8/Screenshot-20200702-124000.png" width="200px" alt="Screenshot-20200702-124000" border="0"></a>
<img src="https://i.ibb.co/v3J7NC5/Screenshot-20200702-124006-1.png" alt="Screenshot-20200702-124006-1" border="0" width = "200px">
<img src="https://i.ibb.co/zGRzMf0/Screenshot-20200702-124021.png" alt="Screenshot-20200702-124021" border="0" width = "200px">
<img src="https://i.ibb.co/t3wLbkr/Screenshot-20200703-002029.png" alt="Screenshot-20200703-002029" border="0" width = "200px">
<img src="https://i.ibb.co/HYMw2bX/Screenshot-20200703-002117.png" alt="Screenshot-20200703-002117" border="0" width = "200px">
<img src="https://i.ibb.co/sPfwb6S/Screenshot-20200713-220830.png" alt="Screenshot-20200713-220830" border="0" width = "200px">
<img src="https://i.ibb.co/PWz1Cwd/Screenshot-20200713-221828.png" alt="Screenshot-20200713-221828" border="0" width = "200px">
